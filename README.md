# google-seo-poc

1. Do npm install in the google-seo-poc directory

2. Install phantomjs and live-server globally using below command: 
   $ npm install -g phantomjs live-server

3. Do ionic serve/build so that the build files gets generated. 

4. Open another terminal and run the following command in google-seo-poc/www folder:
    $ live-server --entry-file=index.html
    You will be shown Serving "<SomeFolders>/gcs-poc-phantom/www" at http://127.0.0.1:8080

5. Now Open another terminal in the folder google-seo-poc and run the following command:
   $ phantomjs --disk-cache=no angular-seo-server.js 8888 http://127.0.0.1:8080

6. Open your browser and hit http://localhost:8888 and you will shown with the html content.

7. If you want to view page source alone, go to view-source in browser or use postman.

Other Sample URL's:
1. http://localhost:8888/movies
2. http://localhost:8888/page2
3. http://localhost:8888/tvseries