import { NgModule, ErrorHandler } from '@angular/core';
import { LocationStrategy, PathLocationStrategy, APP_BASE_HREF } from '@angular/common';
import { IonicApp, IonicModule, IonicErrorHandler, DeepLinkConfig } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { Page3 } from '../pages/page3/page3';
import { Page4 } from '../pages/page4/page4';
import { Page5 } from '../pages/page5/page5';
import { Movies } from '../pages/movies/movies';
import { TVSeries } from '../pages/tv-series/tv-series';
import { SeoService } from '../services/seo.service';

export const deepLinkConfig: DeepLinkConfig = {
  links: [
    { component: Page1, name: 'Page 1', segment: 'page1' },
    { component: Page2, name: 'Page 2', segment: 'page2' },
    { component: Page3, name: 'Page 3', segment: 'page3' },
    { component: Page4, name: 'Page 4', segment: 'page4' },
    { component: Page5, name: 'Page 5', segment: 'page5' },
    { component: Movies, name: 'Movies', segment: 'movies' },
    { component: TVSeries, name: 'TVSeries', segment: 'tvseries' },
  ]
};

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    Page3,
    Page4,
    Page5,
    Movies,
    TVSeries
  ],
  imports: [
    IonicModule.forRoot(MyApp, {}, deepLinkConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    Page3,
    Page4,
    Page5,
    Movies,
    TVSeries
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: APP_BASE_HREF, useValue: "/" },
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    SeoService
  ]
})

export class AppModule {
}
