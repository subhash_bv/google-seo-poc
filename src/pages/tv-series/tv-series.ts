import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SeoService } from '../../services/seo.service';

@Component({
    selector: 'tv-series',
    templateUrl: 'tv-series.html'
})
export class TVSeries implements OnInit {
    constructor(public navCtrl: NavController, private seoService: SeoService) {
        this.seoService.setTitle('TV Series - My POC');
        this.seoService.setMetaDescription('This is tv series');
        this.seoService.setMetaRobots('Index, Follow');
    }

    ngOnInit(): void {
        this.seoService.setTitle('TV Series - My POC');
        this.seoService.setMetaDescription('This is tv series page');
        this.seoService.setMetaRobots('Index, Follow');
    }


    goBack(): void {
        this.navCtrl.pop();
    }
}
