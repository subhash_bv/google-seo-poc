import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SeoService } from '../../services/seo.service';

@Component({
    selector: 'movies',
    templateUrl: 'movies.html'
})
export class Movies implements OnInit {

    constructor(public navCtrl: NavController, private seoService: SeoService) {
    }

    ngOnInit(): void {
        this.seoService.setTitle('Movies - My POC');
        this.seoService.setMetaDescription('This is movies page');
        this.seoService.setMetaRobots('Index, Follow');
    }

    goBack(): void {
        this.navCtrl.pop();
    }
}
