import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SeoService } from '../../services/seo.service';

@Component({
    selector: 'page-page3',
    templateUrl: 'page3.html'
})

export class Page3 implements OnInit {

    constructor(public navCtrl: NavController, private seoService: SeoService) {
    }

    ngOnInit(): void {
        this.seoService.setTitle('Page3 - My POC');
        this.seoService.setMetaDescription('This is page 3');
        this.seoService.setMetaRobots('Index, Follow');
    }


}
