import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SeoService } from '../../services/seo.service';

@Component({
    selector: 'page-page5',
    templateUrl: 'page5.html'
})
export class Page5 implements OnInit {
    constructor(public navCtrl: NavController, private seoService: SeoService) {
    }

    ngOnInit(): void {
        this.seoService.setTitle('Page 5 - My POC');
        this.seoService.setMetaDescription('This is page 5');
        this.seoService.setMetaRobots('Index, Follow');
    }

    goBack(): void {
        this.navCtrl.pop();
    }
}
