import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SeoService } from '../../services/seo.service';
import { Page4 } from '../page4/page4';
import { Page5 } from '../page5/page5';
import { Movies } from '../movies/movies';
import { TVSeries } from '../tv-series/tv-series';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})

export class Page1 implements OnInit {

  constructor(public navCtrl: NavController, private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.setTitle('Page 1 - My POC');
    this.seoService.setMetaDescription('This is page 1');
    this.seoService.setMetaRobots('Index, Follow');
  }


  goToMovies(): void {
    this.navCtrl.push(Movies);
  }

  goToTVSeries(): void {
    this.navCtrl.push(TVSeries);
  }

  goToPage4(): void {
    this.navCtrl.push(Page4);
  }

  goToPage5(): void {
    this.navCtrl.push(Page5);
  }

}
