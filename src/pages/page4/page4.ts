import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SeoService } from '../../services/seo.service';

@Component({
    selector: 'page-page4',
    templateUrl: 'page4.html'
})
export class Page4 implements OnInit {
    constructor(public navCtrl: NavController, private seoService: SeoService) {
    }

    ngOnInit(): void {
        this.seoService.setTitle('Page 4 - My POC');
        this.seoService.setMetaDescription('This is page 4');
        this.seoService.setMetaRobots('Index, Follow');
    }


    goBack(): void {
        this.navCtrl.pop();
    }
}
